<?php
header("Access-Control-Allow-Origin: *");
$mc = new Memcached(); 
$mc->addServer("localhost", 11211); 

if (isset($_REQUEST['ip'])){
  $remote_ip = $_REQUEST['ip'];
} else {
  $remote_ip = $_SERVER['REMOTE_ADDR'];
}

if (!filter_var($remote_ip, FILTER_VALIDATE_IP)) {
  die(json_encode(array('ip' => 'Invalid IP', 'dns' => 'Invalid IP', 'asn' => array(array('AS' => 'Invalid', 'AS Name' => 'Invalid IP')))));
}

$cache = $mc->get($remote_ip . "_info");

if (!$cache) {
  require_once 'Net/Whois.php';
  $whois = new Net_Whois;
  $remote_rdns = gethostbyaddr($remote_ip);
  $remote_asn  = $whois->query($remote_ip, 'v4.whois.cymru.com');
  $mc->set($remote_ip . "_info", array($remote_rdns,$remote_asn), 15*60);
  header('XX-Cache-Hit: false');
} else {
  $remote_rdns = $cache[0];
  $remote_asn  = $cache[1];
  header('XX-Cache-Hit: true');
}
  
$asn_array = array();
$t = explode("\n", $remote_asn);
foreach($t as $i){
  if (isset($headers)) {
    $v = array_combine($headers, array_map('trim', explode("|", $i)));
    if (!!$v) {
      array_push($asn_array, $v);
    }
  } else {
    $headers = array_map('trim', explode("|", $i));
  }
}
$array = array(
  'ip' => $remote_ip,
  'dns' => $remote_rdns,
  'asn' => $asn_array
);
echo(json_encode($array));
