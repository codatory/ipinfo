<?php
header("Access-Control-Allow-Origin: *");
$mc = new Memcached(); 
$mc->addServer("localhost", 11211); 

if (isset($_REQUEST['ip'])){
  $remote_ip = $_REQUEST['ip'];
} else {
  $remote_ip = $_SERVER['REMOTE_ADDR'];
}

if (!filter_var($remote_ip, FILTER_VALIDATE_IP)) {
  die('Invalid IP Address');
}

$cache = $mc->get($remote_ip . "_trace");

if (!$cache) {
  $trace = shell_exec("mtr -enrc 5 '" . escapeshellarg($remote_ip) . "'");
  $mc->set($remote_ip . "_trace", $trace, 15*60);
  header('XX-Cache-Hit: false');
} else {
  $trace = $cache;
  header('XX-Cache-Hit: true');
}

echo $trace;